move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;



//hit by spikes

if (place_meeting(x,y+1,obj_spike)){
key_jump = true;
health = health - 20;
}

// change sprite when move
if (move != 0){
      image_xscale = move;
}

if (grounded){
    if(hsp == 0){
    sprite_index=spr_idle;
    image_speed = 0.05;
    }
    else if (hsp <> 0){
   sprite_index = spr_run;
   image_speed = 0.16;
   }       
}else if (vsp <> 0 && !grounded && !key_jump){
    sprite_index = spr_jump;   
}

//shoot
if (key_shoot){
audio_play_sound(snd_shoot,1,false);
bullet = instance_create(x,y,obj_bullet_megaman);
bullet.speed = image_xscale*5;
if (grounded){
    if(hsp == 0){
    sprite_index=spr_idle_shoot;
    image_speed = 0.05;
    }
    else if (hsp <> 0){
   sprite_index = spr_run_shoot;
   image_speed = 0.16;
   }       
}else if (vsp <> 0 && !grounded && !key_jump){
    sprite_index = spr_jump_shoot;   
}
}


if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
        audio_play_sound(snd_ground,1,false);
    }else if(grounded && key_jump){ //recently jumping
    
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    if (key_right = true){
      
    }else{
    
    } 
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }
}

//ladder
if (key_up || key_down){
    if place_meeting(x,y,obj_par_ladder){
    sprite_index = spr_up_ladder;
    image_speed = 0.16;
    ladder = true;
    }
}

if (ladder){

    vsp = 0 ;
    if (key_up){
    vsp = -2;
    }
    if (key_down){
    vsp = 2;
    }
    if (!place_meeting (x,y,obj_par_ladder)){
    ladder = false
    }
    if (key_jump){
    ladder = false;
    }
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollission = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y,obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;

}
y+= vsp;

//grounded

if (place_meeting(x,y+8,obj_wall)){
    if (fearofheights &&
        !position_meeting (x+(sprite_width/2)*dir, y+(sprite_height/2)+8, obj_wall)){
        
            if (position_meeting (x+(sprite_width*3)*dir, y+(sprite_height/2)+8, obj_wall)){
                key_jump = true;
            }else{
            dir = -dir;
            }
            
        }

}        
